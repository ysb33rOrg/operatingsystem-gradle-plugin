/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2018 - 2024
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.os

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.ysb33r.grolifant5.api.core.OperatingSystem

/**
 * Installs an extension called {@code OS}, which mimics {@link OperatingSystem}.
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.6.0
 */
@CompileStatic
class OperatingSystemPlugin implements Plugin<Project> {

    void apply(Project project) {
        project.extensions.create('OS', OperatingSystemPlugin.Extension)
    }

    static class Extension implements OperatingSystem {
        @Delegate
        private final static OperatingSystem OS = OperatingSystem.current()
    }
}
