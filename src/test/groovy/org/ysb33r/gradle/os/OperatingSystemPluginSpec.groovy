/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2018 - 2024
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.os

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant5.api.core.OperatingSystem
import spock.lang.Specification

class OperatingSystemPluginSpec extends Specification {

    void 'operating system is accessible when plugin is applied'() {
        Project project = ProjectBuilder.builder().build()

        when:
        project.allprojects {
            apply plugin : 'org.ysb33r.os'
        }

        OperatingSystemPlugin.Extension ext = (OperatingSystemPlugin.Extension)project.extensions.getByName('OS')

        then:
        ext.pathVar == OperatingSystem.current().pathVar
    }
}