/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2018 - 2024
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.os

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import spock.lang.Specification
import spock.lang.TempDir

class SimpleSpec extends Specification {

    @TempDir
    File projectDir

    void 'Apply plugin in Kotlin DSL'() {

        given:
        new File(projectDir, 'build.gradle.kts').text = """
        plugins {
            java
            id("org.ysb33r.os")
        }
        """.stripIndent()
        when:
        BuildResult result = GradleRunner.create().withArguments('tasks', '--all')
            .withProjectDir(projectDir)
            .withPluginClasspath().forwardOutput().build()

        then:
        result.output.contains('compileJava')
    }
}